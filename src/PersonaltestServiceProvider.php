<?php

namespace Testing\Personaltest;

use Illuminate\Support\ServiceProvider;

class PersonaltestServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        include __DIR__.'/routes.php';
        $this->app->make('Testing\Personaltest\PersonaltestController');
    }
}
